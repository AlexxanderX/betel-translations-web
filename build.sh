#!/bin/sh
export WEBPACK_OUTPUT="$(pwd)/public"
export WEBPACK_PUBLIC_PATH="/betel-translations-web/"
export WEBPACK_USE_HASH_ROUTER="1"
export NODE_ENV="production"
rm -rf ./public/*
cd ../web
npx webpack
cp -R ./public/* ../web-static/public/
